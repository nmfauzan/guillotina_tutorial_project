import websocket
import json

def on_message(ws,message):
    print(json.dumps(json.loads(message.decode()), indent=4))

def on_error(ws, error):
    print(error)

def on_close(ws, close_status_code, close_msg):
    print('### closed ###')

def on_open(ws):
    print("Open")

if __name__ == "__main__":
    websocket.enableTrace(True)
    token = "eyJhbGciOiJBMjU2S1ciLCJlbmMiOiJBMjU2Q0JDLUhTNTEyIn0.g7QyXODBauqSr6kmVHtoo3W_wMExrKruLC75SKOudd9KWZZG72y7B1_wG1vZ1RY4m76VX5zvq-IOEbZUUflmvWAVlZuVIHjx.ue16H2t2NLYqm7OUGh9-9g.isM9B96t0JtHL4p1OSLK_eeikIfm-QnawY-_foj2jfNFe7v2Fpp02FXuspBB4rJvnFw8UPJaA8u7AOcrS2p8ng.YnVQHbHDI7OuhpHsu1esUB-b13VmQG-1JVjRUPCYw6c"
    ws = websocket.WebSocketApp('ws://localhost:8080/db/container/@conversate?ws_token=' + token,
                                on_open = on_open,
                                on_message = on_message,
                                on_error = on_error,
                                on_close = on_close)
    ws.run_forever()