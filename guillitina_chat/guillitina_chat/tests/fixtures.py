from guillotina import testing
from guillotina.tests.fixtures import ContainerRequesterAsyncContextManager

import json
import pytest


def base_settings_configurator(settings):
    if 'applications' in settings:
        settings['applications'].append('guillitina_chat')
    else:
        settings['applications'] = ['guillitina_chat']


testing.configure_with(base_settings_configurator)


class guillitina_chat_Requester(ContainerRequesterAsyncContextManager):  # noqa

    async def __aenter__(self):
        await super().__aenter__()
        resp = await self.requester(
            'POST', '/db/guillotina/@addons',
            data=json.dumps({
                'id': 'guillitina_chat'
            })
        )
        return self.requester


@pytest.fixture(scope='function')
async def guillitina_chat_requester(guillotina):
    return guillitina_chat_Requester(guillotina)
