import pytest


pytestmark = [pytest.mark.asyncio]


async def test_install(guillitina_chat_requester):  # noqa
    async with guillitina_chat_requester as requester:
        response, _ = await requester('GET', '/db/guillotina/@addons')
        assert 'guillitina_chat' in response['installed']
