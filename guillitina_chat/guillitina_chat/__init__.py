from guillotina import configure


app_settings = {
    "load_utilities": {
        "guillotina_chat.message_sender": {
            "provides": "guillitina_chat.utility.IMessageSender",
            "factory": "guillitina_chat.utility.MessageSenderUtility",
            "settings": {}
        },
    }
}


def includeme(root):
    """
    custom application initialization here
    """
    configure.scan('guillitina_chat.api')
    configure.scan('guillitina_chat.install')
    configure.scan('guillitina_chat.content')
    configure.scan('guillitina_chat.permissions')
    configure.scan('guillitina_chat.subscribers')
    configure.scan('guillitina_chat.serialize')
    configure.scan('guillitina_chat.services')
    configure.scan('guillitina_chat.utility')
    configure.scan('guillitina_chat.ws')
